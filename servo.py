import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

fw_left = 23
fw_right = 24

bw_left = 25
bw_right = 22

GPIO.setup(fw_left, GPIO.OUT)
GPIO.setup(fw_right, GPIO.OUT)
GPIO.setup(bw_left, GPIO.OUT)
GPIO.setup(bw_right, GPIO.OUT)

class WheelController:
    def __init__(self):
        self.delay = 0.02
    
    def reset_wheels(self):
        GPIO.output(bw_left, 0)
        GPIO.output(bw_right, 0)
        GPIO.output(fw_left, 0)
        GPIO.output(fw_right, 0)

    def drive_forward(self):
        self.reset_wheels()
    
        GPIO.output(fw_left, 1)
        GPIO.output(fw_right, 1)
        time.sleep(self.delay)
        GPIO.output(fw_left, 0)
        GPIO.output(fw_right, 0)
        time.sleep(self.delay)

    def drive_backwards(self):
        self.reset_wheels()
    
        GPIO.output(bw_left, 1)
        GPIO.output(bw_right, 1)
        time.sleep(self.delay)
        GPIO.output(bw_left, 0)
        GPIO.output(bw_right, 0)
        time.sleep(self.delay)
