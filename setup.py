"""
     This is a setup module. Define all GPIO constants here
"""

import RPi.GPIO as GPIO

# use GPIO number pattern
GPIO.setmode(GPIO.BCM)

# wheels GPIO
fw_left = 23
fw_right = 24
bw_left = 25
bw_right = 22

GPIO.setup(fw_left, GPIO.OUT)
GPIO.setup(fw_right, GPIO.OUT)
GPIO.setup(bw_left, GPIO.OUT)
GPIO.setup(bw_right, GPIO.OUT)
